# Pages

A markdown-based documentation tool and framework for creating structured documents. Create papers, static web sites or technical manuals and publish them in any format supported by the Tao Note package.